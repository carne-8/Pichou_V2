namespace Types

module NodeJs =

    type IPackageJson =
        {
            version: string
            version_name: string
        }


module PokedexTypes =

    type ISpecies =
        {
            name: string
            url: string
        }

    type IPokeType =
        {
            name: string
            url: string
        }

    type IPokeTypeInList =
        {
            slot: int
            ``type``: IPokeType
        }

    type IPokeStat =
        {
            name: string
            url: string
        }

    type IPokeStatGlobal =
        {
            base_stat: int
            stat: IPokeStat
        }

    type ISpriteDreamWorld =
        {
            front_default: string option
            front_female: string option
        }

    type ISpriteOfficialArtwork =
        {
            front_default: string option
        }

    type IOtherPokeSprites =
        {
            dream_world: ISpriteDreamWorld
            ``official-artwork``: ISpriteOfficialArtwork
        }

    type IPokeSprites =
        {
            back_default: string
            back_female: string option
            back_shiny: string option
            back_shiny_female: string option
            front_default: string
            front_female: string option
            front_shiny: string option
            front_shiny_female: string option
            other: IOtherPokeSprites
        }

    type IPokeAPI =
        {
            species: ISpecies
            name: string
            types: IPokeTypeInList list
            stats: IPokeStatGlobal list
            sprites: IPokeSprites
            height: int
            weight: int
        }


    type ILanguage =
        {
            name: string
            url: string
        }

    type IPokeVersion =
        {
            name: string
            url: string
        }

    type IPokeDescription =
        {
            flavor_text: string
            language: ILanguage
            version: IPokeVersion
        }

    type IPokeAPIDescription =
        {
            flavor_text_entries: IPokeDescription list
        }

module RocketTypes =

    type ILocation =
        {
            id: int
            name: string
            state: string option
            statename: string option
            country: string
            slug: string
        }

    type IPad =
        {
            id: int
            name: string
            location: ILocation
        }

    type IVehicle =
        {
            id: int
            name: string
            company_id: int
            slug: string
        }

    type IProvider =
        {
            id: int
            name: string
            slug: string
        }

    type IMission =
        {
            id: int
            sort_date: string
            name: string
            provider: IProvider
            vehicle: IVehicle
            pad: IPad
            mission_description: string option
            launch_description: string option
            date_str: string
            slug: string
            weather_summary: string option
            weather_temp: float option
            weather_condition: string option
            weather_wind_mph: float option
            weather_icon: string option
            weather_updated: string option
            quicktext: string
            suborbital: bool
            modified: string
        }

    type IRocketAPI =
        {
            result: IMission list
        }

module SncfAPI =
    type IApplicationPeriod =
        {
            ``begin``: string
            ``end``: string
        }

    type IImpactedStops =
        {
            base_departure_time: string option
            amended_departure_time: string option
            base_arrival_time: string option
            amended_arrival_time: string option
        }

    type IImpactedObjects =
        {
            impacted_stops: IImpactedStops list
        }

    type IDisruptions =
        {
            application_periods: IApplicationPeriod list
            impacted_objects: IImpactedObjects list
        }

    type ISncfAPI =
        {
            disruptions: IDisruptions list
        }