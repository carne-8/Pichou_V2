module Rocket

open Fable.Core
open Types.Discord
open Types.RocketTypes
open Thoth.Fetch
open Thoth.Json

type IForRocket =
    {
        time: string
    }


let fetchPromise() : JS.Promise<Result<IRocketAPI, FetchError>> =
    promise {
        return! Fetch.tryGet("https://fdo.rocketlaunch.live/json/launches/next", caseStrategy = SnakeCase)
    }

let fetchTimeOfLaunch (timestamp: string) : JS.Promise<Result<IForRocket, FetchError>> =
    promise {
        let url = sprintf "http://forrocket.titaye.dev/%s" timestamp
        return! Fetch.tryGet(url)
    }


let rocketFunction (discord: IDiscord) (message: IDiscordMsg) =
    promise {
        let! rocketJson = fetchPromise()

        match rocketJson with
        | Ok x ->
            let json = x.result.[0]
            let! time = fetchTimeOfLaunch json.sort_date

            let mutable description = ""

            match time with
            | Ok time ->
                match json.launch_description with
                    | Some x -> description <- x
                    | None -> description <- "Il n'y en a pas."

                let embedParams = {|
                    title = json.name
                    description = time.time
                    color = 11423193
                    fields = [|
                        {|
                            name = "🚀-Rocket"
                            value = json.vehicle.name
                            ``inline`` = false
                        |}
                        {|
                            name = "🏗-Provider"
                            value = json.provider.name
                            ``inline`` = false
                        |}
                        {|
                            name = "🚧-Launch Site"
                            value = json.pad.location.name
                            ``inline`` = false
                        |}
                        {|
                            name = "🌍-Country"
                            value = json.pad.location.country
                            ``inline`` = true
                        |}
                        {|
                            name = "Stay in orbit"
                            value = if json.suborbital then "❌" else "✅"
                            ``inline`` = true
                        |}
                        {|
                            name = "🍦-description"
                            value = description
                            ``inline`` = false
                        |}
                    |]
                |}

                let embed = discord.CreateEmbed(embedParams)

                message.channel.send embed |> ignore
            | Error x ->
                let embed = discord.CreateEmbed()
                embed.setColor("#ff0000")
                embed.addField("Oups !", "Il y a eu une erreur.", true)

                printfn "%A" x

                message.channel.send embed |> ignore
        | Error x ->
            let embed = discord.CreateEmbed()
            embed.setColor("#ff0000")
            embed.addField("Oups !", "Il y a eu une erreur.", true)

            printfn "%A" x

            message.channel.send embed |> ignore
    } |> Promise.start