module SoundBox

open System
open Fable
open Sounds

open Types.Discord

type PouchDBError =
    abstract status: int
    abstract name: string
    abstract message: string
    abstract error: bool

let mutable embedsList: IEmbed list = []

let createEmbedsList (discord: IDiscord) soundBoxSource =
    allSounds |> List.iteri (fun index sound ->
        let imagePath = soundBoxSource + sound.imagePath

        let embed = discord.CreateEmbed()

        embed.setTitle sound.name
        embed.setThumbnail imagePath
        embed.setDescription ( sprintf "Pour jouer se son, écrit %i" (index + 1) )

        let newList = embedsList @ [ embed ]
        embedsList <- newList
    )

let postChoices (discord: IDiscord) (message: IDiscordMsg) (soundNumber: string option) =
    promise {
        match soundNumber with
        | Some soundNumber ->
            match Int32.TryParse(soundNumber) with
            | true, value ->
                if value > Int32.Parse (getEnv "SOUND_COUNT") then
                    let embed = discord.CreateEmbed()
                    embed.setColor("#ff0000")
                    embed.addField("Calme toi !", "Ce son n'éxiste pas !")
                    message.channel.send embed |> ignore
                else
                    let! connection = message.``member``.voice.channel.join()

                    let soundPath = getEnv "SOUNDBOX_SOURCE" + Sounds.allSounds.[value - 1].path

                    let dispatcher = connection.play soundPath

                    dispatcher.on("finish", (fun _ ->
                        connection.disconnect()
                    ))
            | false, _ ->
                let embed = discord.CreateEmbed()
                embed.setColor("#ff0000")
                embed.addField("RTFM !", "Tu dois écrire le numéro du son que tu veux jouer !")
                message.channel.send embed |> ignore
                sprintf "%s/SoundBox.jpg" (getEnv "SOUNDBOX_SOURCE") |> message.channel.send |> ignore
        | None ->
            message.channel.send "Écrit le numéro du son que tu veut jouer." |> ignore
            sprintf "%s/SoundBox.jpg" (getEnv "SOUNDBOX_SOURCE") |> message.channel.send |> ignore

            let authorId = message.author.id

            let filter (responseMessage: IDiscordMsg) =
                responseMessage.author.id = authorId

            let! userResponse = message.channel.awaitMessages(filter, {| max = 1; time = 20000 |})

            match Int32.TryParse(userResponse.first().content) with
            | true, value ->
                if value > Int32.Parse (getEnv "SOUND_COUNT") then
                    let embed = discord.CreateEmbed()
                    embed.setColor("#ff0000")
                    embed.addField("Calme toi !", "Ce son n'éxiste pas !")
                    message.channel.send embed |> ignore
                else
                    let! connection = userResponse.first().``member``.voice.channel.join()

                    let soundPath = getEnv "SOUNDBOX_SOURCE" + Sounds.allSounds.[value - 1].path
                    printfn "%A" soundPath

                    let dispatcher = connection.play soundPath
                    dispatcher.on("error", (fun error ->
                        printfn "Error: %A" error
                    ))

                    dispatcher.on("finish", (fun _ ->
                        connection.disconnect()
                    ))
            | false, _ ->
                let embed = discord.CreateEmbed()
                embed.setColor("#ff0000")
                embed.addField("Zut !", "Tu n'as pas écris un nombre !")
                message.channel.send embed |> ignore
    } |> Promise.catch(fun error -> (
        if error.ToString() = "Error [VOICE_JOIN_CHANNEL]: You do not have permission to join this voice channel." then
            let embed = discord.CreateEmbed()
            embed.setColor("#ff0000")
            embed.addField("JPP !!", "Je n'ai pas la permission pour me connecter au vocal dans lequel tu es !")
            message.channel.send embed |> ignore
        elif error.ToString() = "TypeError: Cannot read property 'join' of null" then
            let embed = discord.CreateEmbed()
            embed.setColor("#ff0000")
            embed.addField("Stop !", "Il faut que tu sois dans un salon vocal pour que je puisse te chuchoter se doux son.")
            message.channel.send embed |> ignore
        elif error.ToString() = "TypeError: Cannot read property 'content' of undefined" then
            let embed = discord.CreateEmbed()
            embed.setColor("#ff0000")
            embed.addField("Tu n'es pas très concentré !", "Le temps est écoulé et pour des raisons de sécurité la procédure est annulée.")
            message.channel.send embed |> ignore
        else
            let embed = discord.CreateEmbed()
            embed.setColor("#ff0000")
            embed.addField("Oups !", sprintf "J'ai eu un petit problème, contacte le S.A.V. si besoin.\n%s" (error.ToString()))
            message.channel.send embed |> ignore
    )) |> Promise.start