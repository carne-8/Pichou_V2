namespace Types.API.Twitter

open Fable.Core

type Tweet =
    { id: string
      text: string }

type ApiResponse =
    { data: Tweet list }